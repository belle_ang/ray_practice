/**
 * 所有的單位統一為 um
 * 使用此物件時, 以 canvas 的 id 來建立 WaferMap 物件, 此物件包括作圖基本資訊, 所有的 Die / Shot / Mask 資料陣列, 以及 tooltip 顯示
 * dieCoords[] = [[x y fill PF]...] , fill:0=不填滿,1=填滿 , PF:0=Pass,1=Fail
 * shotCoords[] = [[x y w h fill freeze]...] , fill:0=不填滿,1=填滿 , freeze:0=Not freeze,1=Freeze
 * maskCoords[] = [[x y w h fill freeze]...] , fill:0=不填滿,1=填滿 , freeze:0=Not freeze,1=Freeze
 */

//宣告一個 WaferMap 物件
function WaferMap(objID) {
	var obj = this;   //給 event listener 內的函式可以定址到 WaferMap 的物件
	var canvas = document.getElementById(objID);
	var cxt = canvas.getContext("2d");
	var imageSize = canvas.width<canvas.height ? canvas.width : canvas.height;   //整個圖檔大小
	var marginPixel = 30;   //四周所要留下的空白 要填寫座標值用的
	var waferSize = 200000;   //所有單位統一為 um
	var dieWidth = 10000;
	var dieHeight = 10000;
	var ringEdge = 3000;
	var pixelSize = waferSize / (imageSize - marginPixel * 2);   //一個pixel代表多少 um
	var dieWidthPixel = dieWidth / pixelSize;
	var dieHeightPixel = dieHeight / pixelSize;
	var dieOffsetX = 0;   //這是以 die 為基準的設定, dieOffset=0,0 表示中央那個Die的中心等於Wafer的中心
	var dieOffsetY = 0;
	var shotOffsetX = 0;   //這是以 shot 為基準的設定, 是從外面指定進來的, 當他為 0,0 時, 表示中央的 Shot 的中心等於 Wafer 的中心, 此時可能導致 dieOffset 就不為 0,0 了
	var shotOffsetY = 0;
	var centerDieX = 0;   //在 Wafer 正中央的那個 Die 的座標
	var centerDieY = 0;
	var orientation = 0;
	var imageCenter = imageSize / 2;   //中心點的 pixel 座標
	var dieCoords = [];   //存放所有的 die 座標集合, x,y,fill,pf
	var shotCoords = [];   //存放所有 shot 的座標集合, x,y,w,h,fill,freeze
	var maskCoords = [];   //存放所有 mask 的座標集合, x,y,w,h,fill,freeze
	var dieXMin = -20;   //有效的 X 座標左邊邊界值
	var dieXMax = 20;
	var dieYMin = -20;
	var dieYMax = 20;
	var draggable = false;   //用以記錄現在滑鼠是不是壓下
	var backupImg=null, backupX=null, backupY=null;

	cxt.translate(0.5,0.5);   //這只能位移一次, 所以在建立物件時執行一次即可, 否則反覆位移, 圖形會整體一直位移
	
	//========== 這邊的 Event Handler 可以由外部定義與執行即可 ===============================
	//-----------------------------------------------------
	//移動滑鼠時顯示 tooltip, 而且不要讓 tooltip 自動消失
	canvas.addEventListener("mousemove", function(evt){
		if(evt.offsetY < 25) return;   //上緣就不用顯示了, 不然會有殘跡
		var xy = obj.getDieCoord(evt.offsetX, evt.offsetY);
		var info = obj.getDieInfo(xy[0], xy[1]);
		info = "X : " + xy[0] + " , Y : " + xy[1] + (info==undefined||info==""?"":" : " + info); 
		if(backupImg!=null && backupX!=null && backupY!=null) {
			cxt.putImageData(backupImg, backupX, backupY);
		}
		backupImg = cxt.getImageData(evt.offsetX, evt.offsetY-20, evt.offsetX+100, evt.offsetY);
		backupX = evt.offsetX;
		backupY = evt.offsetY - 20;
		cxt.fillStyle = "red";
		cxt.fillText(info, evt.offsetX + 5, evt.offsetY- 3);
	});
	
	
	//-----------------------------------------------------
	//滑鼠按鍵壓下
	canvas.addEventListener("mousedown", function(evt){
		draggable = true;   //滑鼠按鍵壓下
	});
	
	
	//-----------------------------------------------------
	//滑鼠按鍵釋放
	canvas.addEventListener("mouseup", function(){
		draggable = false;   //滑鼠按鍵釋放
	});
	
	
	//-----------------------------------------------------
	//離開 canvas 物件時, 就關閉 tooltip
	canvas.addEventListener("mouseout", function(){
		if(backupImg!=null && backupX!=null && backupY!=null) {
			cxt.putImageData(backupImg, backupX, backupY);
		}
	});
	
	
	//-----------------------------------------------------
	//按下滑鼠時執行的事情
	// canvas.addEventListener("click", function(evt){
	// 	alert(obj.getDieCoord(evt.offsetX, evt.offsetY));
	// });
	
	
	//-----------------------------------------------------
	//雙擊滑鼠時執行的事情
	// canvas.addEventListener("dblclick", function(evt){
		
	// });
	//=========================================================================
	
	//-----------------------------------------------------
	//設定新的 Wafer Map 作圖資訊
	this.setAttribute = function(wafSize,dieW,dieH,offX,offY,cenX,cenY,edge,notch,imgSize,border) {   //設定屬性值
		waferSize = parseInt(wafSize);
		dieWidth = parseFloat(dieW);
		dieHeight = parseFloat(dieH);
		shotOffsetX = parseFloat(offX);   //外面指定的是 Center Shot Offset X/Y
		shotOffsetY = parseFloat(offY);
		centerDieX = parseInt(cenX);
		centerDieY = parseInt(cenY);
		ringEdge = parseFloat(edge);
		orientation = parseInt(notch);
		imageSize = parseInt(imgSize);
		marginPixel = parseInt(border);
		imageCenter = imageSize / 2;
		pixelSize = waferSize / (imageSize - marginPixel * 2);
		dieWidthPixel = dieWidth / pixelSize;
		dieHeightPixel = dieHeight / pixelSize;
		dieXMin = centerDieX - Math.ceil(imageSize / dieWidthPixel / 2);
		dieYMin = centerDieY - Math.ceil(imageSize / dieHeightPixel / 2);
		dieXMax = centerDieX + Math.ceil(imageSize / dieWidthPixel / 2);
		dieYMax = centerDieY + Math.ceil(imageSize / dieHeightPixel / 2);
		//暫時設定 shot 層級 == die 層級
		dieOffsetX = shotOffsetX;
		dieOffsetY = shotOffsetY;
	}
	
	
	//-----------------------------------------------------
	//取得 WaferMap 屬性資訊
	this.getAttributeString = function() {
		return "ImageSize=" + imageSize + ", WaferSize=" + waferSize + ", DieSize=" + dieWidth + "x" + dieHeight + ", Offset=" + offsetX + "," + offsetY + ", Center=?,? RingEdge=" + ringEdge + ", Orientation=" + orientation;
	}
	
	//-----------------------------------------------------
	//取得座標範圍
	this.getCoordRange = function() {
		return [dieXMin, dieYMin, dieXMax, dieYMax];
	}
	
	//-----------------------------------------------------
	//判斷這個座標的Die是否為完整Die (根據centerDieXY修正座標偏移量)
	this.isFullDie = function(x,y) {
		//修正偏移量
		x -= centerDieX;
		y -= centerDieY;
		//左上角
		var x1 = (x-0.5)*dieWidth-dieOffsetX;
		var y1 = (y-0.5)*dieHeight-dieOffsetY;
		var r1 = Math.sqrt(Math.pow(x1,2) + Math.pow(y1,2));
		if(r1>waferSize/2-ringEdge) return false;
		//右上角
		var x2 = (x+0.5)*dieWidth-dieOffsetX;
		var y2 = (y-0.5)*dieHeight-dieOffsetY;
		var r2 = Math.sqrt(Math.pow(x2,2) + Math.pow(y2,2));
		if(r2>waferSize/2-ringEdge) return false;
		//左下角
		var x3 = (x-0.5)*dieWidth-dieOffsetX;
		var y3 = (y+0.5)*dieHeight-dieOffsetY;
		var r3 = Math.sqrt(Math.pow(x3,2) + Math.pow(y3,2));
		if(r3>waferSize/2-ringEdge) return false;
		//右下角
		var x4 = (x+0.5)*dieWidth-dieOffsetX;
		var y4 = (y+0.5)*dieHeight-dieOffsetY;
		var r4 = Math.sqrt(Math.pow(x4,2) + Math.pow(y4,2));
		if(r4>waferSize/2-ringEdge) return false;
		//屬於完整Die
		return true;
	}
	
	
	//-----------------------------------------------------
	//判斷這個光罩內有多少個完整 Die
	this.getFullDieCountInReticle = function(x,y,w,h) {
		var count = 0;
		for(i=0; i<w; i++) {
			for(j=0; j<h; j++) {
				if(this.isFullDie(x+i, y+j)) count ++;
			}
		}
		return count;
	}
	
	
	//判斷這個光罩內有多少個 dieCoords 裡面的 Die
	this.getDieCountInReticle = function(x,y,w,h) {
		var count = 0;
		for(var i=0; i<dieCoords.length; i++) {
			var coord = dieCoords[i];
			if(coord[0]>=x && coord[0]<x+w && coord[1]>=y && coord[1]<y+h) count++;
		}
		return count;
	}
	
	
	//-----------------------------------------------------
	//取得 Die 在 Wafer 上的面積, 完全在 Wafer 內為100, 完全在 Wafer 外為 0
	this.getDieCoverage = function(x,y) {
		//修正偏移量
		x -= centerDieX;
		y -= centerDieY;
		var insideCorner = 0;
		//左上角
		var x1 = (x-0.5)*dieWidth-dieOffsetX;
		var y1 = (y-0.5)*dieHeight-dieOffsetY;
		var r1 = Math.sqrt(Math.pow(x1,2) + Math.pow(y1,2));
		if(r1 <= waferSize/2-ringEdge) insideCorner ++;
		//右上角
		var x2 = (x+0.5)*dieWidth-dieOffsetX;
		var y2 = (y-0.5)*dieHeight-dieOffsetY;
		var r2 = Math.sqrt(Math.pow(x2,2) + Math.pow(y2,2));
		if(r2 <= waferSize/2-ringEdge) insideCorner ++;
		//左下角
		var x3 = (x-0.5)*dieWidth-dieOffsetX;
		var y3 = (y+0.5)*dieHeight-dieOffsetY;
		var r3 = Math.sqrt(Math.pow(x3,2) + Math.pow(y3,2));
		if(r3 <= waferSize/2-ringEdge) insideCorner ++;
		//右下角
		var x4 = (x+0.5)*dieWidth-dieOffsetX;
		var y4 = (y+0.5)*dieHeight-dieOffsetY;
		var r4 = Math.sqrt(Math.pow(x4,2) + Math.pow(y4,2));
		if(r4 <= waferSize/2-ringEdge) insideCorner ++;
		//完全在內會是在外Die
		if(insideCorner == 4) return 100;
		if(insideCorner == 0) return 0;
		//不完整 Die
		var percentage = 0;
		var stepX = (x2 - x1) / 10;
		var stepY = (y3 - y1) / 10;
		for(var dotX=x1+stepX/2; dotX<x2; dotX+=stepX) {
			for(var dotY=y1+stepY/2; dotY<y3; dotY+=stepY) {
				var dotR = Math.sqrt(Math.pow(dotX,2) + Math.pow(dotY,2));
				if(dotR <= waferSize/2-ringEdge) percentage ++;
			}
		}
		if(percentage >= 100) percentage = 99;
		if(percentage <= 0) percentage = 1;
		return percentage;
	}
	
	
	//-----------------------------------------------------
	//取得單一DIE的左上右下的繪圖座標 (根據centerDieXY修正座標偏移量)
	this.getDieRect = function(x,y) {
		//修正偏移量
		x -= centerDieX;
		y -= centerDieY;
		//左上角
		var x1 = imageCenter + ((x-0.5)*dieWidth-dieOffsetX) / pixelSize;
		var y1 = imageCenter + ((y-0.5)*dieHeight-dieOffsetY) / pixelSize;
		//右下角
		var x2 = imageCenter + ((x+0.5)*dieWidth-dieOffsetX) / pixelSize;
		var y2 = imageCenter + ((y+0.5)*dieHeight-dieOffsetY) / pixelSize;
		//計算左上角座標與區塊的長寬
		var result = [Math.round(x1)+1, Math.round(y1)+1, Math.round(x2)-Math.round(x1)-2, Math.round(y2)-Math.round(y1)-2];  //內縮, 不要跟框線重疊
		return result;
	}
	
	
	//-----------------------------------------------------
	//取得光罩的左上右下座標 (根據centerDieXY修正座標偏移量)
	this.getReticle = function(x,y,w,h) {
		//修正偏移量
		x -= centerDieX;
		y -= centerDieY;
		//左上角
		var x1 = imageCenter + ((x-0.5)*dieWidth-dieOffsetX) / pixelSize;
		var y1 = imageCenter + ((y-0.5)*dieHeight-dieOffsetY) / pixelSize;
		//右下角
		var x2 = imageCenter + ((x+w-0.5)*dieWidth-dieOffsetX) / pixelSize;
		var y2 = imageCenter + ((y+h-0.5)*dieHeight-dieOffsetY) / pixelSize;
		//計算左上角座標與區塊的長寬
		var result = [Math.round(x1)+1, Math.round(y1)+1, Math.round(x2)-Math.round(x1)-2, Math.round(y2)-Math.round(y1)-2];  //內縮, 不要跟框線重疊
		return result;
	}
	
	
	//-----------------------------------------------------
	//根據滑鼠座標取得 Die 座標 (根據centerDieXY修正座標偏移量)
	this.getDieCoord = function(mouseX,mouseY) {
		var baseX = imageCenter - (dieWidth/2+dieOffsetX)/pixelSize;
		var baseY = imageCenter - (dieHeight/2+dieOffsetY)/pixelSize;
		var x = Math.floor((mouseX-baseX)/dieWidthPixel) + centerDieX;   //減上偏移量centerDieXY
		var y = Math.floor((mouseY-baseY)/dieHeightPixel) + centerDieY;
		var result = [x, -y];   //第四象限作法, 所以Y加上負號
		return result;
	}
	
	
	//取得指定 Die 的訊息內容
	this.getDieInfo = function(dieX, dieY) {
		if(dieCoords == undefined) return;
		for(var i=0; i<dieCoords.length; i++) {
			var die = dieCoords[i];
			if(die[0]==dieX && die[1]==dieY) {
				return die[4]==undefined ? "" : die[4];
			}
		}
		return "";
	}
	
	
	//-----------------------------------------------------
	//繪製底圖
	this.drawBaseMap = function() {
		if(canvas && canvas.getContext) {
			//取消掉備份的影像區塊
			backupImg = null;
			backupX = null;
			backupY = null;
			//清除底圖
			cxt.beginPath();
			cxt.fillStyle = "#ffffff";
			cxt.fillRect(0,0,imageSize,imageSize);
			cxt.closePath();
			//畫 Wafer 底圖
			cxt.beginPath();
			cxt.fillStyle = "#dddddd";
			cxt.arc(imageCenter,imageCenter,imageCenter-marginPixel,0,2*Math.PI);   //整片 Wafer 的範圍
			cxt.fill();
			cxt.closePath();
			cxt.beginPath();
			cxt.fillStyle = "#eeeeee";
			cxt.arc(imageCenter,imageCenter,imageCenter-marginPixel-ringEdge/pixelSize,0,2*Math.PI);   //扣掉 RingEdge 的範圍
			cxt.fill();
			cxt.closePath();
			//畫 Notch / Orientation 缺角
			cxt.beginPath();
			cxt.fillStyle = "white";
			var x = orientation==90  ? imageSize-marginPixel : orientation==270 ? marginPixel : imageCenter;
			var y = orientation==180 ? imageSize-marginPixel : orientation==0   ? marginPixel : imageCenter;
			cxt.arc(x,y,3000/pixelSize,0,2*Math.PI);
			cxt.fill();
			cxt.closePath();
			//繪製框線
			cxt.beginPath();
			cxt.setLineDash([0,0]);   //實線
			cxt.strokeStyle = "black";
			cxt.lineWidth = 1;
			cxt.strokeRect(marginPixel,marginPixel,imageSize-2*marginPixel,imageSize-2*marginPixel);
			cxt.closePath();
			//設定接下來的是虛線
			cxt.setLineDash([2,2]);
			//畫水平線
			cxt.font = "12px verdana";   //verdana,arial,sans-serif
			var yGrids = Math.round(waferSize / dieHeight / 2);
			var dieHeightShiftPixel = Math.round(dieHeight/pixelSize/2);   //這個參數用來調整座標label的位置
			for(i=-yGrids; i<=yGrids; i++) {
				var y = Math.round(imageCenter + ((i + 0.5) * dieHeight - dieOffsetY) / pixelSize);
				if(y>marginPixel && y<imageSize-marginPixel) {
					cxt.beginPath();
					cxt.moveTo(marginPixel,y);
					cxt.lineTo(imageSize-marginPixel,y);
					cxt.stroke();
					cxt.fillStyle = "black";
					label = (-i + centerDieY).toString();  //第一象限, 所以Y位置要加上負號
					cxt.fillText(label, marginPixel-label.length*6-4,y-dieHeightShiftPixel+5);   //6是字體的寬度,5是字體一半的高度
					cxt.closePath()
				}
			}
			//畫垂直線
			var xGrids = Math.round(waferSize / dieWidth / 2);
			var dieWidthShiftPixel = Math.round(dieWidth/pixelSize/2);
			for(i=-xGrids; i<=xGrids; i++) {
				var x = Math.round(imageCenter + ((i - 0.5) * dieWidth - dieOffsetX) / pixelSize);
				if(x>marginPixel && x<imageSize-marginPixel) {
					cxt.beginPath();
					cxt.moveTo(x, marginPixel);
					cxt.lineTo(x, imageSize-marginPixel);
					cxt.stroke();
					cxt.fillStyle = "black";
					label = (i + centerDieX).toString();
					cxt.fillText(label, x+dieWidthShiftPixel-label.length*5,imageSize-marginPixel+12);
					cxt.closePath();
				}
			}
			//標示圓心
			cxt.beginPath();
			cxt.strokeStyle = "red";
			cxt.moveTo(imageCenter, imageCenter-3);
			cxt.lineTo(imageCenter, imageCenter+3);
			cxt.moveTo(imageCenter-3, imageCenter);
			cxt.lineTo(imageCenter+3, imageCenter);
			cxt.stroke();
			cxt.closePath();
		}
	}
	
	
	//-----------------------------------------------------
	//畫一顆Die(P=綠色,F=青色)
	this.drawDie = function(x,y,label,isFill,pf) {
		if(x==null || y==null || x.length==0 || y.length==0 || isNaN(x) || isNaN(y)) return;   //沒有有效的座標的就略過
		//取得座標
		var rect = this.getDieRect(x,-y);   //第四象限, Y軸加上負號
		cxt.beginPath();
		//填滿
		if(isFill) {
			cxt.fillStyle = pf ? "rgba(0,255,0,0.15)" : "rgba(196,0,0,0.15)";
			cxt.fillRect(rect[0], rect[1], rect[2], rect[3]);
		}
		//框線
		cxt.setLineDash([0,0]);
		cxt.strokeStyle = pf ? "rgba(0,255,0,0.5)" : "rgba(196,0,0,0.5)";
		cxt.rect(rect[0], rect[1], rect[2], rect[3]);
		cxt.stroke();
		cxt.closePath();
		//標籤字串
		if(label!=undefined && label.length>0) {
			cxt.fillStyle = "#0000ff";
			cxt.fillText(label, rect[0]+rect[2]/2-label.length*4,rect[1]+rect[3]/2+5);
		}
	}
	
	
	//-----------------------------------------------------
	//畫一個Shot(藍色)
	this.drawShot = function(x,y,w,h,str,isFill,isFreeze) {
		//取得座標
		var rect = this.getReticle(x,y,w,h);
		cxt.beginPath();
		//填滿
		if(isFill) {
			cxt.fillStyle = isFreeze ? "rgba(0,0,255,0.25)" : "rgba(0,0,255,0.15)";
			cxt.fillRect(rect[0], rect[1], rect[2], rect[3]);
		}
		//框線
		cxt.setLineDash([0,0]);
		cxt.strokeStyle = "rgba(0,0,255,0.5)";
		cxt.rect(rect[0], rect[1], rect[2], rect[3]);
		cxt.stroke();
		//凍結
//		if(isFreeze) {
//			cxt.moveTo(rect[0],rect[1]);
//			cxt.lineTo(rect[0]+rect[2],rect[1]+rect[3]);
//			cxt.stroke();
//			cxt.moveTo(rect[0],rect[1]+rect[3]);
//			cxt.lineTo(rect[0]+rect[2],rect[1]);
//			cxt.stroke();
//		}
		//標示文字
		cxt.fillStyle = "#0000ff";
		cxt.fillText(str, rect[0]+rect[2]-str.length*6-3,rect[1]+10);
		cxt.closePath();
	}
	
	
	//-----------------------------------------------------
	//畫一個Mask(紅)
	this.drawMask = function(x,y,w,h,str,isFill,isFreeze) {
		//取得座標
		var rect = this.getReticle(x,y,w,h);
		cxt.beginPath();
		//填滿
		if(isFill) {
			cxt.fillStyle = isFreeze ? "rgba(255,0,0,0.25)" : "rgba(255,0,0,0.15)";
			cxt.fillRect(rect[0], rect[1], rect[2], rect[3]);
		}
		//框線
		cxt.setLineDash([0,0]);
		cxt.strokeStyle = "rgba(255,0,0,0.5)";
		cxt.rect(rect[0], rect[1], rect[2], rect[3]);
		cxt.stroke();
		//凍結
//		if(isFreeze) {
//			cxt.moveTo(rect[0],rect[1]);
//			cxt.lineTo(rect[0]+rect[2],rect[1]+rect[3]);
//			cxt.stroke();
//			cxt.moveTo(rect[0],rect[1]+rect[3]);
//			cxt.lineTo(rect[0]+rect[2],rect[1]);
//			cxt.stroke();
//		}
		//標示文字
		cxt.fillStyle = "#ff0000";
		cxt.fillText(str, rect[0]+2,rect[1]+10);
		cxt.closePath();
	}
	
	
	//-----------------------------------------------------
	//繪製 dieCoords 內的所有圖形
	this.drawAllDie = function() {
		for(var i=0; i<dieCoords.length; i++) {
			var coord = dieCoords[i];
			var x = coord[0];
			var y = coord[1];
			var fill = coord.length>2 && coord[2]==0 ? false : true;
			var pf = coord.length>3 && coord[3]==1 ? false : true;
			this.drawDie(x, y, fill, pf);
		}
	}
	
	
	//繪製 shotCoords 內的所有圖形
	this.drawAllShot = function() {
		for(var i=0; i<shotCoords.length; i++) {
			var coord = shotCoords[i];
			var x = coord[0];
			var y = coord[1];
			var w = coord[2];
			var h = coord[3];
			var fill = coord.length>4 && coord[4]==0 ? false : true;
			var freeze = coord.length>5 && coord[5]==1 ? true : false;
			var dieCount = this.getDieCountInReticle(x, y, w, h);
			this.drawShot(x, y, w, h, dieCount.toString(), fill, freeze);
		}
	}
	
	
	//繪製 maskCoords 內的所有圖形
	this.drawAllMask = function() {
		for(var i=0; i<maskCoords.length; i++) {
			var coord = maskCoords[i];
			var x = coord[0];
			var y = coord[1];
			var w = coord[2];
			var h = coord[3];
			var fill = coord.length>4 && coord[4]==0 ? false : true;
			var freeze = coord.length>5 && coord[5]==1 ? true : false;
			var dieCount = this.getDieCountInReticle(x, y, w, h);
			this.drawMask(x, y, w, h, dieCount.toString(), fill, freeze);
		}
	}
	
	
	//重新繪製全部內容, 依序為:底圖,Die,Shot,Mask
	this.drawAll = function() {
		this.drawBaseMap();
		this.drawAllDie();
		this.drawAllShot();
		this.drawAllMask();
	}
	
	
	//-----------------------------------------------------
	//自動產生所有有效 die 的清單
	this.genDefaultDieSet = function() {
		dieCoords.length = 0;
		for(x=dieXMin; x<=dieXMax; x++) {
			for(y=dieYMin; y<=dieYMax; y++) {
				if(this.isFullDie(x, y)) {
					dieCoords.push([x,y,1,0]);
				}
			}
		}
	}
	
	
	/**
	 * 依據 shotOffsetXY 來自動建立預設的 Shot Layout
	 * 方法是根據 wafer attribute 先確認 center shot 的位置, 然後再推估起始位置, 擴展開來
	 */
	this.genDefaultShotSet = function(stepX, stepY) {
		var stepX = parseInt(stepX);
		var stepY = parseInt(stepY);
		//由 centerShot 的 offset 值來換算出 centerDie 的 offset 值
		dieOffsetX = shotOffsetX;
		dieOffsetY = shotOffsetY;
		if(stepX % 2 == 0) dieOffsetX -= 0.5 * dieWidth;
		if(stepY % 2 == 0) dieOffsetY -= 0.5 * dieHeight;
		//計算 Shot 左上角的 Die 座標
		var startX = centerDieX - Math.floor(stepX / 2);   
		var startY = centerDieY - Math.floor(stepY / 2);
		//求得 Shot 分布最左上角的起始座標, 然後展開來
		shotCoords.length = 0;
		while(startX + stepX >= dieXMin) startX -= stepX;
		while(startY + stepY >= dieYMin) startY -= stepY;
		for(var x=startX; x<=dieXMax+stepX; x+=stepX) {
			for(var y=startY; y<=dieYMax+stepY; y+=stepY) {
				var cnt = this.getFullDieCountInReticle(x, y, stepX, stepY);
				if(cnt > 0) {
					shotCoords.push([x,y,stepX,stepY,1]);
				}
			}
		}
	}
	
	
	/**
	 * 根據 shot 為基礎, 來佈建 mask 陣列
	 * 方法是隨便抓一個 shot 資料, 以此為基礎, 向外擴展長出 mask, 然後挑選長出最少 mask 的為結果
	 */
	this.genDefaultMaskSet = function(multiX, multiY) {
		//先檢查 shotCoords 是否有資料, 然後取得第一個 shot 的資料, 得到座標與長寬資料
		if(shotCoords.length == 0) {
			alert("必須先有 Shot 資料才可以建立預設的 Mask 資料");
			return;
		}
		var coord = shotCoords[0];
		var shotX = coord[0];
		var shotY = coord[1];
		var shotWidth = coord[2];
		var shotHeight = coord[3];
		//設定初始變數
		multiX = parseInt(multiX);
		multiY = parseInt(multiY);
		var maskWidth = shotWidth * multiX;
		var maskHeight = shotHeight * multiY;
		var best = [];   //用以紀錄最佳(最少)的排法
		var curr = [];   //用以記錄目前的排法
		for(var stepX=0; stepX<multiX; stepX++) {
			for(var stepY=0; stepY<multiY; stepY++) {
				//先找尋 startX/Y
				var startX = shotX - stepX * shotWidth;
				var startY = shotY - stepY * shotHeight;
				while(startX >= dieXMin - shotWidth * multiX) startX -= shotWidth * multiX;
				while(startY >= dieYMin - shotHeight * multiY) startY -= shotHeight * multiY;
				curr.length = 0;
				for(var x=startX; x<=dieXMax+maskWidth; x+=maskWidth) {
					for(var y=startY; y<=dieYMax+maskHeight; y+=maskHeight) {
						var cnt = this.getFullDieCountInReticle(x, y, maskWidth, maskHeight);
						if(cnt > 0) {
							curr.push([x,y,maskWidth,maskHeight,1]);
						}
					}
				}
				if(best.length==0 || best.length>curr.length) best = curr;
			}
		}
		maskCoords = best;
	}
	
	
	//-----------------------------------------------------
	//清除 die coord set
	this.clearDieCoords = function() {
		dieCoords.length = 0;
	}
	
	
	//清除 shot coord set
	this.clearShotCoords = function() {
		shotCoords.length = 0;
	}
	
	
	//清除 mask coord set
	this.clearMaskCoords = function() {
		maskCoords.length = 0;
	}
	
	
	//取得 dieCoords 資料陣列
	this.getDieCoords = function() {
		return dieCoords;
	}
	
	
	//取得 shotCoords 資料陣列
	this.getShotCoords = function() {
		return shotCoords;
	}
	
	
	//取得 maskCoords 資料陣列
	this.getMaskCoords = function() {
		return maskCoords;
	}
	
	
	//設定 dieCoords 資料陣列
	this.setDieCoords = function(coords) {
		dieCoords = coords;
	}
	
	
	//設定 shotCoords 資料陣列
	this.setShotCoords = function(coords) {
		shotCoords = coords;
	}
	
	
	//設定 maskCoords 資料陣列
	this.setMaskCoords = function(coords) {
		maskCoords = coords;
	}
	
	
	//取得目前 dieCoords 的資料個數
	this.getDieCount = function() {
		return dieCoords.length;
	}
	
	
	//取得目前 shotCoords 的資料個數
	this.getShotCount = function() {
		return shotCoords.length;
	}
	
	
	//取得目前 maskCoords 的資料個數
	this.getMaskCount = function() {
		return maskCoords.length;
	}
	
	
	//-----------------------------------------------------
	//加 Die 或更新 Die, pf:false=Pass,true=Fail
	this.addDie = function(x, y, pf) {
		//先找看看 dieCoords 裡面有沒有
		var dieExist = false;
		for(var i=0; i<dieCoords.length; i++) {
			var coord = dieCoords[i];
			if(coord[0]==x && coord[1]==y) {
				//有找到, 要變更裡面的狀態
				dieCoords[i] = [x,y,1,pf?1:0];
				dieExist = true;
				break;
			}
		}
		if(dieExist == false) {
			dieCoords.push([x,y,1,pf?1:0]);
		}
	}
	
	
	//刪除指定的 Die
	this.delDie = function(x, y) {
		//先找看看 dieCoords 裡面有沒有
		for(var i=0; i<dieCoords.length; i++) {
			var coord = dieCoords[i];
			if(coord[0]==x && coord[1]==y) {
				dieCoords.splice(i, 1);
				break;
			}
		}
	}
	
	
	//增加 Shot 或更新 Shot
	this.addShot = function(x, y, w, h) {
		//先檢查指定的位置會不會壓到其他的 shot
		var conflict = false;
		for(var i=0; i<shotCoords.length; i++) {
			var coord = shotCoords[i];
			if(x>=coord[0] && x<coord[0]+coord[2] && y>=coord[1] && y<coord[1]+coord[3]) conflict = true;   //檢查左上角是否壓到
			if(x+w-1>=coord[0] && x+w-1<coord[0]+coord[2] && y>=coord[1] && y<coord[1]+coord[3]) conflict = true;   //檢查右上角是否壓到
			if(x>=coord[0] && x<coord[0]+coord[2] && y+h-1>=coord[1] && y+h-1<coord[1]+coord[3]) conflict = true;   //檢查左下角是否壓到
			if(x+w-1>=coord[0] && x+w-1<coord[0]+coord[2] && y+h-1>=coord[1] && y+h-1<coord[1]+coord[3]) conflict = true;   //檢查右下角是否壓到
			if(conflict) {
				alert("壓到其他 Shot 了");
				return;
			}
		}
		shotCoords.push([x,y,w,h,1,0]);
	}
	
	
	//刪除 Shot
	this.delShot = function(x, y) {
		for(var i=0; i<shotCoords.length; i++) {
			var coord = shotCoords[i];
			if(x>=coord[0] && x<coord[0]+coord[2] && y>=coord[1] && y<coord[1]+coord[3]) {
				shotCoords.splice(i, 1);
				break;
			}
		}
	}
	
	
	//增加 Mask 或更新 Mask
	this.addMask = function(x, y, w, h) {
		//先檢查指定的位置會不會壓到其他的 mask
		var conflict = false;
		for(var i=0; i<maskCoords.length; i++) {
			var coord = maskCoords[i];
			if(x>=coord[0] && x<coord[0]+coord[2] && y>=coord[1] && y<coord[1]+coord[3]) conflict = true;   //檢查左上角是否壓到
			if(x+w-1>=coord[0] && x+w-1<coord[0]+coord[2] && y>=coord[1] && y<coord[1]+coord[3]) conflict = true;   //檢查右上角是否壓到
			if(x>=coord[0] && x<coord[0]+coord[2] && y+h-1>=coord[1] && y+h-1<coord[1]+coord[3]) conflict = true;   //檢查左下角是否壓到
			if(x+w-1>=coord[0] && x+w-1<coord[0]+coord[2] && y+h-1>=coord[1] && y+h-1<coord[1]+coord[3]) conflict = true;   //檢查右下角是否壓到
			if(conflict) {
				alert("壓到其他 Mask 了");
				return;
			}
		}
		maskCoords.push([x,y,w,h,1,0]);
	}
	
	
	//刪除 Mask
	this.delMask = function(x, y) {
		for(var i=0; i<maskCoords.length; i++) {
			var coord = maskCoords[i];
			if(x>=coord[0] && x<coord[0]+coord[2] && y>=coord[1] && y<coord[1]+coord[3]) {
				maskCoords.splice(i, 1);
				break;
			}
		}
	}

	
	//設定 Mask 的 Freeze 狀態
	this.addFreezeMask = function(x, y) {
		for(var i=0; i<maskCoords.length; i++) {
			var maskCoord = maskCoords[i];
			var mx = maskCoord[0];
			var my = maskCoord[1];
			var mw = maskCoord[2];
			var mh = maskCoord[3];
			var fill = maskCoord[4];
			var freeze = maskCoord[5];
			if(x>=mx && x<mx+mw-1 && y>=my && y<my+mh-1) {
				maskCoords[i] = [mx,my,mw,mh,fill,1];
				break;
			}
		}
	}
	
	//取消 Mask 的 Freeze 狀態
	this.delFreezeMask = function(x, y) {
		for(var i=0; i<maskCoords.length; i++) {
			var maskCoord = maskCoords[i];
			var mx = maskCoord[0];
			var my = maskCoord[1];
			var mw = maskCoord[2];
			var mh = maskCoord[3];
			var fill = maskCoord[4];
			var freeze = maskCoord[5];
			if(x>=mx && x<mx+mw-1 && y>=my && y<my+mh-1) {
				maskCoords[i] = [mx,my,mw,mh,fill,0];
				break;
			}
		}
	}
	
	
	//-----------------------------------------------------
	//針對指定的物件, 座標, 行列, 向前移動數個 die 位置
	this.shiftForward = function(target, x, y, direction, step) {
		if(target == "Die") this.shiftDie(x,y,direction,step);
		if(target == "Shot") this.shiftShot(x,y,direction,step);
		if(target == "Mask") this.shiftMask(x,y,direction,step);
		if(target == "AllDie") this.shiftAllDie(x,y,direction,step);
		if(target == "AllShot") this.shiftAllShot(x,y,direction,step);
		if(target == "AllMask") this.shiftAllMask(x,y,direction,step);
	}
	
	
	//針對指定的物件, 座標, 行列, 向前移動數個 die 位置
	this.shiftBackward = function(target, x, y, direction, step) {
		if(target == "Die") this.shiftDie(x,y,direction,0-step);
		if(target == "Shot") this.shiftShot(x,y,direction,0-step);
		if(target == "Mask") this.shiftMask(x,y,direction,0-step);
		if(target == "AllDie") this.shiftAllDie(x,y,direction,0-step);
		if(target == "AllShot") this.shiftAllShot(x,y,direction,0-step);
		if(target == "AllMask") this.shiftAllMask(x,y,direction,0-step);
	}
	
	
	//移動 Die, direction:0=水平,1=垂直
	this.shiftDie = function(x, y, direction, step) {
		for(var i=0; i<dieCoords.length; i++) {
			var dieCoord = dieCoords[i];
			if(direction==0 && dieCoord[1]==y) {
				dieCoords[i] = [dieCoord[0]+step, dieCoord[1], dieCoord[2], dieCoord[3]];
			} else if(direction==1 && dieCoord[0]==x) {
				dieCoords[i] = [dieCoord[0], dieCoord[1]+step, dieCoord[2], dieCoord[3]];
			}
		}
	}
	
	
	//移動全部的 Die, direction:0=水平,1=垂直
	this.shiftAllDie = function(x, y, direction, step) {
		for(var i=0; i<dieCoords.length; i++) {
			var dieCoord = dieCoords[i];
			if(direction==0) {
				dieCoords[i] = [dieCoord[0]+step, dieCoord[1], dieCoord[2], dieCoord[3]];
			} else if(direction==1) {
				dieCoords[i] = [dieCoord[0], dieCoord[1]+step, dieCoord[2], dieCoord[3]];
			}
		}
	}
	
	
	//移動 Shot, direction:0=水平,1=垂直
	this.shiftShot = function(x, y, direction, step) {
		//先找出所在位置的 shot 資訊
		var currentShot = null;
		for(var i=0; i<shotCoords.length; i++) {
			var shotCoord = shotCoords[i];
			var sx = shotCoord[0];
			var sy = shotCoord[1];
			var sw = shotCoord[2];
			var sh = shotCoord[3];
			if(x>=sx && x<sx+sw && y>=sy && y<sy+sh) {
				currentShot = shotCoord;
			}
		}
		//檢查此一路徑上的 shot 是否都靠齊
		var xgrids = dieXMax - dieXMin;
		var ygrids = dieYMax - dieYMin;
		if(direction == 0) {   //檢查水平
			for(var sx=dieXMin-xgrids; sx<=dieXMax+xgrids; sx+=currentShot[2]) {
				var indices = this.findShotCoordIndices(sx, currentShot[1]);
				if(indices.length == 0) continue;
				for(var i=0; i<indices.length; i++) {
					var refShotCoord = shotCoords[indices[i]];
					if(refShotCoord[1] != currentShot[1]) {
						//如果 shot 的 Y 位置不同
						alert("移動的 Shot 並不在同一個水平位置上, 無法整行移動");
						return;
					}
				}
			}
		} else if(direction == 1) {   //檢查垂直
			for(var sy=dieYMin-ygrids; sy<=dieYMax+ygrids; sy+=currentShot[3]) {
				var indices = this.findShotCoordIndices(currentShot[0], sy);
				if(indices.length == 0) continue;
				for(var i=0; i<indices.length; i++) {
					var refShotCoord = shotCoords[indices[i]];
					if(refShotCoord[0] != currentShot[0]) {
						//如果 shot 的 X 位置不同
						alert("移動的 Shot 並不在同一個垂直位置上, 無法整列移動");
						return;
					}
				}
			}
		}
		//移動此路徑上的 shot
		if(direction == 0) {   //水平移動
			for(var sx=dieXMin-xgrids; sx<=dieXMax+xgrids; sx+=currentShot[2]) {
				var indices = this.findShotCoordIndices(sx, currentShot[1]);
				if(indices.length == 0) continue;
				for(var i=0; i<indices.length; i++) {
					var shotCoord = shotCoords[indices[i]];
					shotCoords[indices[i]] = [shotCoord[0]+step, shotCoord[1], shotCoord[2], shotCoord[3], shotCoord[4], shotCoord[5]];
				}
			}
		} else if(direction == 1) {   //垂直移動
			for(var sy=dieYMin-ygrids; sy<=dieYMax+ygrids; sy+=currentShot[3]) {
				var indices = this.findShotCoordIndices(currentShot[0], sy);
				if(indices.length == 0) continue;
				for(var i=0; i<indices.length; i++) {
					var shotCoord = shotCoords[indices[i]];
					shotCoords[indices[i]] = [shotCoord[0], shotCoord[1]+step, shotCoord[2], shotCoord[3], shotCoord[4], shotCoord[5]];
				}
			}
		}
	}
	
	
	//移動全部的 Shot, direction:0=水平,1=垂直
	this.shiftAllShot = function(x, y, direction, step) {
		for(var i=0; i<shotCoords.length; i++) {
			var shotCoord = shotCoords[i];   //x,y,w,h,fill,freeze
			if(direction==0) {
				shotCoords[i] = [shotCoord[0]+step, shotCoord[1], shotCoord[2], shotCoord[3], shotCoord[4], shotCoord[5]];
			} else if(direction==1) {
				shotCoords[i] = [shotCoord[0], shotCoord[1]+step, shotCoord[2], shotCoord[3], shotCoord[4], shotCoord[5]];
			}
		}
	}
	
	
	//移動 Mask, direction:
	this.shiftMask = function(x, y, direction, step) {
		//先找出所在位置的 mask 資訊
		var currentMask = null;
		for(var i=0; i<maskCoords.length; i++) {
			var maskCoord = maskCoords[i];
			var mx = maskCoord[0];
			var my = maskCoord[1];
			var mw = maskCoord[2];
			var mh = maskCoord[3];
			if(x>=mx && x<mx+mw && y>=my && y<my+mh) {
				currentMask = maskCoord;
			}
		}
		//檢查此一路徑上的 mask 是否都靠齊, 是否沒有凍結的光罩
		var xgrids = dieXMax - dieXMin;
		var ygrids = dieYMax - dieYMin;
		if(direction == 0) {   //檢查水平
			for(var mx=dieXMin-xgrids; mx<=dieXMax+xgrids; mx+=currentMask[2]) {
				var indices = this.findMaskCoordIndices(mx, currentMask[1]);
				if(indices.length == 0) continue;
				for(var i=0; i<indices.length; i++) {
					var refMaskCoord = maskCoords[indices[i]];
					if(refMaskCoord[1] != currentMask[1]) {
						//如果 mask 的 Y 位置不同
						alert("移動的 Mask 並不在同一個水平位置上, 無法整行移動");
						return;
					}
					if(refMaskCoord[5]==1) {
						alert("Mask(" + refMaskCoord[0] + "," + refMaskCoord[1] + ";" + refMaskCoord[2] + ":" + refMaskCoord[3] + ") 是凍結的, 無法整行移動");
						return;
					}
				}
			}
		} else if(direction == 1) {   //檢查垂直
			for(var my=dieYMin-ygrids; my<=dieYMax+ygrids; my+=currentMask[3]) {
				var indices = this.findMaskCoordIndices(currentMask[0], my);
				if(indices.length == 0) continue;
				for(var i=0; i<indices.length; i++) {
					var refMaskCoord = maskCoords[indices[i]];
					if(refMaskCoord[0] != currentMask[0]) {
						//如果 mask 的 X 位置不同
						alert("移動的 Mask 並不在同一個垂直位置上, 無法整列移動");
						return;
					}
					if(refMaskCoord[5]==1) {
						alert("Mask(" + refMaskCoord[0] + "," + refMaskCoord[1] + ";" + refMaskCoord[2] + ":" + refMaskCoord[3] + ") 是凍結的, 無法整列移動");
						return;
					}
				}
			}
		}
		//移動此路徑上的 mask
		if(direction == 0) {   //水平移動
			for(var mx=dieXMin-xgrids; mx<=dieXMax+xgrids; mx+=currentMask[2]) {
				var indices = this.findMaskCoordIndices(mx, currentMask[1]);
				if(indices.length == 0) continue;
				for(var i=0; i<indices.length; i++) {
					var maskCoord = maskCoords[indices[i]];
					maskCoords[indices[i]] = [maskCoord[0]+step, maskCoord[1], maskCoord[2], maskCoord[3], maskCoord[4], maskCoord[5]];
				}
			}
		} else if(direction == 1) {   //垂直移動
			for(var my=dieYMin-ygrids; my<=dieYMax+ygrids; my+=currentMask[3]) {
				var indices = this.findMaskCoordIndices(currentMask[0], my);
				if(indices.length == 0) continue;
				for(var i=0; i<indices.length; i++) {
					var maskCoord = maskCoords[indices[i]];
					maskCoords[indices[i]] = [maskCoord[0], maskCoord[1]+step, maskCoord[2], maskCoord[3], maskCoord[4], maskCoord[5]];
				}
			}
		}
	}
	//移動全部的 Shot, direction:0=水平,1=垂直
	this.shiftAllMask = function(x, y, direction, step) {
		//先檢查有沒有凍結的光罩, 如果有, 就無法移動
		for(var i=0; i<maskCoords.length; i++) {
			var maskCoord = maskCoords[i];
			if(maskCoord[5]!=undefined && maskCoord[5]==1) {
				alert("有凍結的光罩存在, 無法整體移動");
				return;
			}
		}
		for(var i=0; i<maskCoords.length; i++) {
			var maskCoord = maskCoords[i];   //x,y,w,h,fill,freeze
			if(direction==0) {
				maskCoords[i] = [maskCoord[0]+step, maskCoord[1], maskCoord[2], maskCoord[3], maskCoord[4], maskCoord[5]];
			} else if(direction==1) {
				maskCoords[i] = [maskCoord[0], maskCoord[1]+step, maskCoord[2], maskCoord[3], maskCoord[4], maskCoord[5]];
			}
		}
	}
	
	
	//-----------------------------------------------------
	//找出指定位置的 Die 資料
	this.findDieCoordIndices = function(x,y) {
		var indices = [];
		for(var i=0; i<dieCoords.length; i++) {
			var dieCoord = dieCoords[i];
			if(x==dieCoord[0] && y==dieCoord[1]) indices.push(i);
		}
		return indices;
	}
	
	
	//找出指定位置的 Shot 資料
	this.findShotCoordIndices = function(x,y) {
		var indices = [];
		for(var i=0; i<shotCoords.length; i++) {
			var shotCoord = shotCoords[i];
			var sx = shotCoord[0];
			var sy = shotCoord[1];
			var sw = shotCoord[2];
			var sh = shotCoord[3];
			if(x>=sx && x<sx+sw && y>=sy && y<sy+sh) indices.push(i);
		}
		return indices;
	}
	
	
	//找出指定位置的 Mask 資料
	this.findMaskCoordIndices = function(x,y) {
		var indices = [];
		for(var i=0; i<maskCoords.length; i++) {
			var maskCoord = maskCoords[i];
			var mx = maskCoord[0];
			var my = maskCoord[1];
			var mw = maskCoord[2];
			var mh = maskCoord[3];
			if(x>=mx && x<mx+mw && y>=my && y<my+mh) indices.push(i);
		}
		return indices;
	}
	
	
	//-----------------------------------------------------
	//增加 Ugly Die, 若 extendType=ugly 就看 percentage, 若 extendType=outside 就看 outside
	this.makeExtendDie = function(extendType, percentage, outside) {
		//如果是選 ugly 的擴展, 直接計算每個 Die 的 coverage 即可判斷
		if(extendType == "ugly") {
			for(var x=dieXMin; x<=dieXMax; x++) {
				for(var y=dieYMin; y<=dieYMax; y++) {
					//已經存在的就不用管了
					var exist = false;
					for(var i=0; i<dieCoords.length; i++) {
						var dieCoord = dieCoords[i];
						if(x==dieCoord[0] && y==dieCoord[1]) {
							exist = true;
							break;
						}
					}
					if(exist) continue;
					//找出大於等於指定比例的 Ugly Die
					var pct = this.getDieCoverage(x,y);
					if(pct >= percentage) {
						dieCoords.push([x, y, 1, 1]);
					}
				}
			}
		}
		//建立二維陣列, 並將現有的 dieCoords 資料記錄此二維陣列上, 這個在 Outside 及後續的 PassDie 之間的擴展, 都需要用到
		var map = [];
		for(var y=dieYMin; y<=dieYMax; y++) map[y] = [];   //用來建立 map 二維陣列
		for(var i=0; i<dieCoords.length; i++) {
			var dieCoord = dieCoords[i];
			var x = dieCoord[0];
			var y = dieCoord[1];
			var pf = dieCoord[3];   //0=Pass,1=Fail
			map[y][x] = pf;
		}
		//如果是選擇 outside 的擴展, 就利用 map 陣列, 水平與垂直都檢查並擴展, 並且要回填對應的 map 陣列
		if(extendType.startsWith("outside")) {
			//先看水平
			if(extendType=="outside" || extendType=="outsideH") {
				for(var y=dieYMin; y<=dieYMax; y++) {
					var min = dieXMax;
					var max = dieXMin;
					for(var x=dieXMin; x<=dieXMax; x++) {
						if(map[y][x] == 0) {
							if(min > x) min = x;
							if(max < x) max = x;
						}
					}
					if(min > max) continue;   //此一 row 沒有有效 Die
					for(var i=1; i<=outside; i++) {
						if(map[y][min-i] == undefined) {
							map[y][min-i] = 1;
							dieCoords.push([min-i, y, 1, 1]);
						}
					}
					for(var i=1; i<=outside; i++) {
						if(map[y][max+i] == undefined) {
							map[y][max+i] = 1;
							dieCoords.push([max+i, y, 1, 1]);
						}
					}
				}
			}
			//再看垂直
			if(extendType=="outside" || extendType=="outsideV") {
				for(var x=dieXMin; x<=dieXMax; x++) {
					//逐一 column 找出有效 Die 的 Y 值上下界線
					var min = dieYMax;
					var max = dieYMin;
					for(var y=dieYMin; y<=dieYMax; y++) {
						if(map[y][x] == 0) {
							if(min > y) min = y;
							if(max < y) max = y;
						}
					}
					if(min > max) continue;   //此一 column 沒有有效 Die
					for(var i=1; i<=outside; i++) {
						if(map[min-i][x] == undefined) {
							map[min-i][x] = 1;
							dieCoords.push([x, min-i, 1, 1]);
						}
					}
					for(var i=1; i<=outside; i++) {
						if(map[max+i][x] == undefined) {
							map[max+i][x] = 1;
							dieCoords.push([x, max+i, 1, 1]);
						}
					}
				}
			}
		}
		//再把兩邊都是 Pass Die 的中間有沒選的全選上
		//先看水平, 填補兩個 Pass Die 中間的 empty
		for(var y=dieYMin; y<=dieYMax; y++) {
			var min = dieXMax;
			var max = dieXMin;
			for(var x=dieXMin; x<=dieXMax; x++) {
				if(map[y][x] == 0) {
					if(min > x) min = x;
					if(max < x) max = x;
				}
			}
			if(min < max) {
				for(var i=min+1; i<max; i++) {
					if(map[y][i] == undefined) {
						map[y][i] = 1;
						dieCoords.push([i, y, 1, 1]);
					}
				}
			}
		}
		//再看垂直, 填補兩個 Pass Die 中間的 empty
		for(var x=dieXMin; x<=dieXMax; x++) {
			var min = dieYMax;
			var max = dieYMin;
			for(var y=dieYMin; y<=dieYMax; y++) {
				if(map[y][x] == 0) {
					if(min > y) min = y;
					if(max < y) max = y;
				}
			}
			if(min < max) {
				for(var i=min+1; i<max; i++) {
					if(map[i][x] == undefined) {
						map[i][x] = 1;
						dieCoords.push([x, i, 1, 1]);
					}
				}
			}
		}
	}
	
	
	//清除 extend die : 目前是判斷為 Fail Die 就清掉
	this.clearExtendDie = function() {
		for(var i=0; i<dieCoords.length; i++) {
			var dieCoord = dieCoords[i];
			var x = dieCoord[0];
			var y = dieCoord[1];
			var pf = dieCoord[3];
			if(pf == 0) continue;
//			if(this.isFullDie(x,y) == false) {    //到底是看是否為完整Die還是看是否為FailDie
				dieCoords.splice(i, 1);
				i --;
//			}
		}
	}
	
	
	//清除 freeze mask 資料
	this.clearFreezeMask = function() {
		for(var i=0; i<maskCoords.length; i++) {
			var maskCoord = maskCoords[i];
			var freeze = maskCoord[5];
			if(freeze == 0) continue;
			var x = maskCoord[0];
			var y = maskCoord[1];
			var w = maskCoord[2];
			var h = maskCoord[3];
			var fill = maskCoord[4];
			maskCoords[i] = [x,y,w,h,fill,0];
		}
	}
}
