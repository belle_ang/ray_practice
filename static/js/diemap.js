/**
 * 所有的距離單位為 um
 * 使用此物件時, 以 canvas 的 id 來建立 DieMap, 此物件包括作圖基本資訊
 * 
 * 
 * 
 */
function DieMap(objID) {
    var obj = this;
    var canvas = document.getElementById(objID);
    var cxt = canvas.getContext("2d");
    var imageWidth = canvas.width, imageHeight = canvas.height;
    var xMin=null, xMax=null, yMin=null, yMax=null;
    var pixelSize = 1;
    var marginPixel = 50;
    var moduleList = null;   //json資料串列
    var ratio = 1;   //放大倍率, 1~10
    
    cxt.translate(0.5, 0.5);

    //========== 這邊的 Event Handler 可以由外部定義與執行即可 ===============================
	//-----------------------------------------------------
    //移動滑鼠時顯示 tooltip, 而且不要讓 tooltip 自動消失
	canvas.addEventListener("mousemove", function(evt){
		var x = evt.pageX + 20;
		var y = evt.pageY + 20;
		// var xy = obj.getDieCoord(evt.offsetX, evt.offsetY);
		// var info = obj.getDieInfo(xy[0], xy[1]);
		var info = "DieMap..."; 
		// $('#' + objID).jqxTooltip({content:info, autoHide:false, position:'absolute', absolutePositionX:x, absolutePositionY:y});
	});

	
	//-----------------------------------------------------
	//滑鼠按鍵壓下
	canvas.addEventListener("mousedown", function(evt){
		draggable = true;   //滑鼠按鍵壓下
	});
	
	
	//-----------------------------------------------------
	//滑鼠按鍵釋放
	canvas.addEventListener("mouseup", function(){
		draggable = false;   //滑鼠按鍵釋放
	});
	
	
	//-----------------------------------------------------
	//離開 canvas 物件時, 就關閉 tooltip
	canvas.addEventListener("mouseout", function(){
		// $('#' + objID).jqxTooltip('close',500);
	});
	
	
	//-----------------------------------------------------
	//按下滑鼠時執行的事情
	canvas.addEventListener("click", function(evt){
	});
	
	
	//-----------------------------------------------------
	//雙擊滑鼠時執行的事情
	canvas.addEventListener("dblclick", function(evt){
		
    });
    
    //-----------------------------------------------------
    //捲動滑鼠滾輪
    canvas.addEventListener("mousewheel", function(event){
        if(event.ctrlKey) {
            //有壓下 ctrl 鍵, 進行 ratio 的調整並重新繪圖
            if(event.wheelDelta > 0 && ratio < 9) {
                //zoom-in
                ratio ++;
                obj.calculateDrawInfo();
                obj.draw();
            } else if(event.wheelDelta < 0 && ratio > 1) {
                //zoom-out
                ratio --;
                obj.calculateDrawInfo();
                obj.draw();
            }
            //防止上層網頁進行 ctrl+wheel 的動作(網頁zoom in/out)
            event.preventDefault();
        }
    });
    //=========================================================================
    //設定 module 的 json 資料, [{'mrk':1,'mod':'TL1234XY','x':0,'y':0},...]
    this.setModuleList = function(data) {
        moduleList = data;
    }


    //計算繪圖資訊, [{'mrk':1,'mod':'TL1234XY','x':0,'y':0},...] , mrk:0='',1=#
    this.calculateDrawInfo = function() {
        if(moduleList == undefined || moduleList == null) return;
        //取得座標範圍
        xMin=null;
        xMax=null;
        yMin=null;
        yMax=null;
        for(var i=0; i<moduleList.length; i++) {
            var module = moduleList[i];
            if(xMin==null || xMin>module['x']) xMin = module['x'];
            if(xMax==null || xMax<module['x']) xMax = module['x'];
            if(yMin==null || yMin>module['y']) yMin = module['y'];
            if(yMax==null || yMax<module['y']) yMax = module['y'];
        }
        //決定圖檔的長寬
        var width=500, height=500;
        var aspectRatio = (xMax-xMin)/(yMax-yMin);   //長寬比
        if(aspectRatio >= 1) {
            //寬的圖
            height /= aspectRatio;
        } else {
            //長的圖
            width *= aspectRatio;
        }
        //加上邊框並乘上放大比例
        imageWidth = width * ratio + marginPixel * 2;
        imageHeight = height * ratio + marginPixel * 2;
        canvas.width = imageWidth;
        canvas.height = imageHeight;
        //計算一個點的值
        pixelSize = (xMax - xMin) / (width * ratio);
    }

    //繪製 die map
    this.draw = function() {
        if(canvas && canvas.getContext) {
            //清除底圖
			cxt.beginPath();
			cxt.fillStyle = "#ffffff";
			cxt.fillRect(0,0,imageWidth,imageHeight);
            cxt.closePath();
            //畫框線
            cxt.beginPath();
			cxt.setLineDash([0,0]);   //實線
			cxt.strokeStyle = "black";
			cxt.lineWidth = 1;
			cxt.strokeRect(marginPixel,marginPixel,imageWidth-2*marginPixel,imageHeight-2*marginPixel);
            cxt.closePath();
            //逐一 module 繪製
            for(var i=0; moduleList!=null && i<moduleList.length; i++) {
                var module = moduleList[i];
                var mrk = module['mrk'];
                var name = module['mod'];
                var x = module['x'];
                var y = module['y'];
                if(isNaN(x) || isNaN(y)) continue;
                x = marginPixel + ((x - xMin) / pixelSize);
                y = marginPixel + ((y - yMin) / pixelSize);
                //繪製座標點
                cxt.beginPath();
                cxt.setLineDash([0,0]);
                cxt.strokeStyle = mrk==0 ? "blue" : "gray";
                cxt.lineWidth = 2;
                cxt.strokeRect(x-2, y-2, 5, 5);
                cxt.closePath();
                //module名稱
                cxt.beginPath();
                cxt.fillStyle = "red";
                cxt.font = "12px verdana";
                cxt.textAlign = "center";
                cxt.fillText(name, x, y-5);
                cxt.closePath();
            }
        }
    }

    
    


}