## Table of Content

### Database

#### Connection

```python
import mariadb
db_conn = mariadb.connect(user='')





```

#### CRUD

1. SELECT
   - SELECT <column's title> from <table's name> WHERE <column's title> = '目標名稱'

2.　 INSERT - insert 會因為要加入的資料行, 後面會夾住許多參數 - INSERT INTO <table's name>('column 的 title_1', 'title2', 'titile3'...) VALUES(?, ?, ? ) - cursor.excuete 可以帶參數 : cursor.execute("第一個 db 的 command", "第二個, 要帶進去的參數"
) - 日期或時間 now() - 在第一個用 ? 取代待會要填值的位置

3. UPDATE

   - `UPDATE <table's name> SET <column1>=<條件> WHERE <column? = ''`

4. DELETE
   - DELETE FROM <table's name> WHERE <column's name>=? ,
   - 如果用 cursor.excute(), 後面可以加參數

### 撈出 DB 的資料

- 1. 先用某個關鍵字 library id 去 父 table 做檢查(是檢查說父條件有沒有在?) , ex: library table
- 2. 再用 library id 去 子 table 撈出全部 dict, ex: 用 lib_id 去 algos table 找出整個 dict , dict 的 key 對應 handsontable 的 data 裡面的 key
- 3. cursor return 會是 list of list , 用 map 去 split 他們
- 4. datetime 格式裝進去 json 會出問題, 所以要先把 date 轉成相容的 string format

### Blueprint

1. local.py

```python
from flask import Blurprint
handle_tmp = Blueprint ('url_name', __name__)


@handle_tmp('', methods = ['GET','POST'])
def a_function():
    return render_template('urls/aaa.html', args = parameter)
```

2. app.py

```python
# REGISTER
app.register_blueprint(handle_tmp, url_prefix="/name_of_fileDir")
```

## loadData Function

- url : 指定執行的 python 路徑
- data :用 json {'cmd': {{sno}}}
-

![](2021-08-10-10-15-43.png)
