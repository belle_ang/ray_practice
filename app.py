from flask import Flask, render_template, request
from four import db_four
from accounts import user_accounts
from xyz import xyz

app = Flask(__name__)
app.register_blueprint(db_four, url_prefix="/db_four")
app.register_blueprint(user_accounts, url_prefix="/user_accounts")
app.register_blueprint(xyz, url_prefix="/xyz")


@app.route("/")
@app.route("/1")
def one():
    return render_template("one.html")


@app.route("/2")
def two():
    return render_template("two.html")


@app.route("/3")
def three():
    return render_template("three.html")


@app.route("/4")
def four():
    return render_template("four.html")


@app.route("/convertObject", methods=["GET", "POST"])
def convertObject():
    print("ooxx")
    # 資料可以成功送出去; 資料怎麼收進來
    f_name = request.form.get("filename")
    print("*** read :", f_name)

    return f"server reply:'{f_name}'"


@app.route("/db_query", methods=["GET", "POST"])
def data_query():
    _cmd = request.form.get("_cmd")
    # print ( f"\n***{datetime.datetime.now()}- {_cmd}\n")
    # print ("***" +_data)
    return f"app.py return : <{_cmd}>"


# a = data_query("SELECT User FROM mysql.User")
# print (a)
if __name__ == "__main__":
    app.run(debug=True, port="1122")
