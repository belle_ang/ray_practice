import mariadb
from flask import Blueprint, request, jsonify


db_four = Blueprint("db_four", __name__)


@db_four.route("/create", methods=["POST", "GET"])
def data_create(_cmd):
    print(f" db : {_cmd}")
    dbconn = mariadb.connect(user="ray", password="ray123", host="localhost", port=3306, database="tmp")
    cursor = dbconn.cursor()
    # cursor.execute("SELECT * FROM tmp_user WHERE sno=1111")
    cursor.execute(_cmd)
    cursor.commit()
    cursor.close()
    return "Insert Done!"


@db_four.route("/update", methods=["GET", "POST"])
def data_update(_cmd):
    print(f" db : {_cmd}")
    dbconn = mariadb.connect(user="ray", password="ray123", host="localhost", port=3306, database="tmp")
    cursor = dbconn.cursor()
    cursor.execute(_cmd)
    cursor.commit()
    cursor.close()
    return "Updated!"


@db_four.route("/delete", methods=["GET", "POST"])
def data_delete(_cmd):
    print(f" db : {_cmd}")
    dbconn = mariadb.connect(user="ray", password="ray123", host="localhost", port=3306, database="tmp")
    cursor = dbconn.cursor()
    cursor.execute(_cmd)
    cursor.commit()
    cursor.close()
    return "Deleted!"


@db_four.route("/query", methods=["GET", "POST"])
def data_query(_cmd):
    print(f" db : {_cmd}")
    dbconn = mariadb.connect(user="ray", password="ray123", host="localhost", port=3306, database="tmp")
    cursor = dbconn.cursor()
    cursor.execute(_cmd)
    _ret = cursor.fetchall()
    cursor.close()
    return _ret


@db_four.route("/db_query", methods=["POST", "GET"])
def query_test():
    _cmd = request.form.get("_cmd")
    # 把command 塞進去db
    dbconn = mariadb.connect(user="ray", password="ray123", host="localhost", port=3306, database="tmp")
    cursor = dbconn.cursor()
    cursor.execute(_cmd)
    _ret = cursor.fetchall()
    cursor.close()
    return f"***{_ret}"


@db_four.route("/delete_sno", methods=["POST", "GET"])
def delete_sno():
    sno = request.args.get("sno")
    dbconn = mariadb.connect(user="ray", password="ray123", host="localhost", port=3306, database="tmp")
    cursor = dbconn.cursor()
    cursor.execute(f"DELETE FROM tmp.tmp_user WHERE sno=" + sno)

    print("**** HELLO ****")
    dbconn.commit()
    cursor.close()
    return f'"{sno}" deleted!'


@db_four.route("loadDataFromServer", methods=["POST", "GET"])
def query_tmp_table():
    dbconn = mariadb.connect(user="ray", password="ray123", host="localhost", port=3306, database="tmp")
    cursor = dbconn.cursor()
    cursor.execute("SELECT sno, tester_id, date_format(log_time,'%Y-%m-%d %H:%i:%s') FROM tmp.tmp_user")
    ret = cursor.fetchall()
    cursor.close()

    # sno, tester_id,log_time  = map(list, zip(*ret))
    # # format convet for datetime
    # dict_a = {
    #     'sno':sno,
    #     'tester_id': tester_id,
    #     'log_time':log_time
    #     }

    data = []
    for row in ret:
        d = {}
        d["sno"] = row[0]
        d["tester_id"] = row[1]
        d["log_time"] = row[2]

        data.append(d)
    return jsonify(data)


@db_four.route("/insert_new_data", methods=["POST", "GET"])
def insert_new_column():
    sno = request.form.get("sno")
    # todo
    pass


@db_four.route("/xyz", methods=["POST", "GET"])
def print_text():

    return "mko0nji9"


if __name__ == "__main__":
    pass
